import "./InvitationTableRow.css";
import React from "react";
import HoverableComponent from "../../../components/HoverableComponent";
import TooltippedButton from "../../../components/Tooltipped/TooltippedButton";
import moment from "moment";
import { Link } from "react-router-dom";

class InvitationTableRow extends HoverableComponent {
  render() {
    const invitation = this.props.invitation;
    console.log(invitation);
    return (
      <tr
        className="event-row"
        onMouseEnter={this.handleMouseEnter}
        onMouseLeave={this.handleMouseLeave}
      >
        <td>{invitation.eventName}</td>
        <td>{invitation.eventPlace}</td>
        <td>{moment(invitation.eventDate).format("YYYY-MM-DD HH:mm")}</td>
        <td>
          {invitation.invitation.inventoryAsked.length > 0 ? "Yes" : "No"}
        </td>
        <td>
          {invitation.invitation.response
            ? (this.state.isHovering && (
                <Link to={`/event/${invitation.invitation.eventId}`}>Show</Link>
              )) || <span className="row-button">Show</span>
            : this.renderResponseButtons(invitation.invitation._id)}
        </td>
      </tr>
    );
  }

  renderResponseButtons(id) {
    return (
      <div>
        <TooltippedButton
          icon="cancel"
          className="btn-floating btn-small waves-effect waves-light pink right response-button"
          title="Reject invitation"
          onClick={() => this.props.responseCall(id, false)}
        />
        <TooltippedButton
          icon="check"
          className="btn-floating btn-small waves-effect waves-light cyan right response-button"
          title="Accept invitation"
          onClick={() => this.props.responseCall(id, true)}
        />
      </div>
    );
  }
}

export default InvitationTableRow;
